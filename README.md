# sermon uploads

This is a script to help with uploading sermons to ACC's site. It handles converting the WAV file from the recording to an MP3, amplifying it (by a fixed best-guess amount), setting the ID3 tags and image on the track, and trimming stuff we don't need from the start and end of the recording.

## Prerequisites

You'll need a linux machine (or _maybe_ a Mac?). It was setup and tested on a Raspberry Pi running Raspbian 8. (It occasionally overheated while processing this script, installing a heat sink seems to have helped)

You'll need to listen to sound it plays - eg plug in headphones, or a speaker, or connect via HDMI to a monitor with speakers built in

A login for the ACC Wordpress account

The password for SFTP access to the server

## Setup Steps:

* install Ruby
* install required libraries:
```
sudo apt-get install lame
sudo apt-get install vlc
sudo apt-get install ffmpeg
sudo apt-get install libavtools
```

* clone the repo
`git clone https://gitlab.com/segunn/sermon-uploads.git`

* symlink the file to somewhere on your $PATH to make life easier (change path to file as appropriate)
`sudo ln -s /home/pi/projects/sermon-uploads/upload_a_sermon.rb bin/sermon_upload`

* you may need to change the permissions to make the file executable (again, change path to file as appropriate)
`chmod +x /home/pi/projects/sermon-uploads/upload_a_sermon.rb`

## Uploading a sermon

* plug in the USB stick or MP3 recorder
* `sermon_upload /media/pi/path_to_the_recording_file.wav`
* answer the questions as prompted (speaker, passage etc)
* after a while, the recording will start playing. Use arrow keys to jump left and right until you know what time you want the recording to start and stop. Press `q` to exit the playback, then answer the prompts to trim unwanted stuff from the start and end
* when the script tries to copy the file to the server, you'll be prompted for the FTP password
* once it's done, copy the printed out URL for the file and use it to fill in the WordPress new sermon form at aigburthcommunitychurch.org/acc/wp-admin

## Edge cases

There are some cases it can't handle (sorry!)

* if someone didn't stop the recording, so it just ended when the amp was turned off, this script won't be able to handle the file format. You'll need to use the program Audacity and select "Import -> Raw Data" (instead of WAV file) and then it should successfully open it
* if there are two recorded files (eg sermon and questions) it won't handle sticking them together
* if there is a single recording that we need to cut the middle out of (eg sermon, song and questions) you'll need to do this manually
