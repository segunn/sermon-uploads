#!/usr/bin/ruby
require 'fileutils'
require 'date'

begin

filename = ARGV.shift
ARGV.clear

if filename.nil?
  puts "What is the path to the file? (Breaks with spaces!!)"
  filename = gets.chomp
end

if filename =~ /.mp3/
  file_type = 'mp3'
  puts "\nAssuming this is an mp3 file"
elsif filename =~ /.wav/
  file_type = 'wav'
  puts "\nAssuming this is a wav file"
else
  puts "\nWhat is the file type? (wav or mp3)"
  file_type = gets.chomp
end

puts "\nWho spoke?"
preacher = gets.chomp

puts "\nWhat was the passage?"
passage = gets.chomp

puts "\nDoes the track need amplifying? (Y/N)"
should_amplify = gets.chomp == 'Y'

puts "\nIs this sermon from today? (Y/N)"
is_today = gets.chomp == 'Y'
if is_today
  date = Date.today.to_s
else
  puts "\nWhat was the date? (YYYY-MM-DD)"
  date = gets.chomp
end

puts "\n\nUploading the recording from when #{preacher} spoke on #{passage} on #{date}..."

puts "-> Making a copy of the file..."
new_filename_base = "./#{date.gsub('-', '.')}.#{passage.gsub(/\s/, '').gsub(':','.')}"
starting_filename = new_filename_base + "_initial." + file_type
mp3_filename = new_filename_base + '_encoded.mp3'
final_filename = new_filename_base + '.mp3'

FileUtils.cp(filename, starting_filename)

puts "-> Encoding as MP3..."
`lame --tt "#{passage}" --ta "#{preacher}" --tl "Aigburth Community Church" --ty "#{date.split('-').first}" --ti "/home/pi/projects/sermon-uploads/acclogo_colour.jpg" --quiet #{'--scale 10' if should_amplify} #{starting_filename} #{mp3_filename}`

system("nvlc #{mp3_filename}")

puts "\nDo you need to trim any of the ends of the recording? (Y/N)"
do_trim = gets.chomp == 'Y'

if do_trim

  puts "\nWhen do you want the recording to start? (HH:MM:SS)"
  trim_from_beginning = gets.chomp

  puts "\nWhen do you want the recording to end? (HH:MM:SS)"
  end_at_time = gets.chomp

  start_h, start_m, start_s = trim_from_beginning.split(':').map(&:to_i)
  end_h, end_m, end_s = end_at_time.split(':').map(&:to_i)
  duration = (end_h*60*60 + end_m*60 + end_s) - (start_h*60*60 + start_m*60 + start_s)

  `avconv -i #{mp3_filename} -ss #{trim_from_beginning} -acodec copy -t #{duration} #{final_filename}`

else

 FileUtils.cp(mp3_filename, final_filename)

end

puts "-> Uploading to the server"
`echo "put #{final_filename}" | sftp u48513347-sermon@home237492709.1and1-data.host`

puts "The file has been copied to the server, copy the following path into the wordpress form:"
puts "\n\nhttp://www.aigburthcommunitychurch.org/acc/wp-content/uploads/podcast/#{final_filename.gsub('./','')}"

rescue => e

  puts e
  puts e.backtrace

ensure


puts "\n Do you want to clear up the files? (Y/N)"
do_clear_up = gets.chomp == 'Y'

if do_clear_up

  `rm #{starting_filename}`
  `rm #{mp3_filename}`
  `rm #{final_filename}`

  puts "All done!"
end

end
